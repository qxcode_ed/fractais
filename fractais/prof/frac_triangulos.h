#ifndef FRAC_TRIANGULOS_H
#define FRAC_TRIANGULOS_H
#include "lib/pen.h"

void tri_facil(Pen &p, int tam){
    if(tam < 4)
        return;
    for(int i = 0; i < 3; i++){
        p.walk(tam);
        p.left(120);
        tri_facil(p, tam/2);
    }
}

void frac_triangulo(){
    Pen pen(800, 800);
    pen.setSpeed(60);
    pen.setThickness(1);
    pen.setColor(255, 255, 255);
    pen.setXY(50, 750);
    pen.setHeading(0);
    //sierpinski(Par(100, 700), Par(350, 200), Par(600, 700), pen, 6);
    tri_facil(pen, 700);
    pen.wait();
}

#endif // FRAC_TRIANGULOS_H
